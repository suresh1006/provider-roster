import org.apache.spark.sql.{SparkSession, DataFrame}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

// Initialize Spark Session
val spark = SparkSession.builder()
  .appName("Visits Analysis")
  .config("spark.master", "local")
  .getOrCreate()

// Define schema for providers.csv
val providersSchema = StructType(Array(
  StructField("provider_id", IntegerType, true),
  StructField("provider_specialty", StringType, true),
  StructField("first_name", StringType, true),
  StructField("middle_name", StringType, true),
  StructField("last_name", StringType, true)
))

// Load providers.csv
val providersDF = spark.read
  .option("header", "true")
  .option("delimiter", "|")
  .schema(providersSchema)
  .csv("/mnt/data/providers.csv")

// Define schema for visits.csv
val visitsSchema = StructType(Array(
  StructField("visit_id", IntegerType, true),
  StructField("provider_id", IntegerType, true),
  StructField("visit_date", StringType, true)
))

// Load visits.csv
val visitsDF = spark.read
  .option("header", "false")
  .schema(visitsSchema)
  .csv("/mnt/data/visits.csv")
  .withColumn("visit_date", to_date(col("visit_date"), "yyyy-MM-dd"))

// Merge the dataframes on provider_id
val mergedDF = visitsDF.join(providersDF, "provider_id")

// Task 1: Calculate the total number of visits per provider, partitioned by specialty
val visitsPerProvider = mergedDF.groupBy("provider_id", "provider_specialty", "first_name", "middle_name", "last_name")
  .agg(count("visit_id").alias("visit_count"))
  .orderBy("provider_specialty", "provider_id")

// Convert the result to JSON format and save to file
val visitsPerProviderJson = visitsPerProvider
  .groupBy("provider_specialty")
  .agg(collect_list(struct("provider_id", "first_name", "middle_name", "last_name", "visit_count")).alias("providers"))
  .toJSON
visitsPerProviderJson.write.mode("overwrite").json("visits_per_provider.json")

// Task 2: Calculate the total number of visits per provider per month
val visitsPerProviderMonth = visitsDF.withColumn("visit_month", date_format(col("visit_date"), "yyyy-MM"))
  .groupBy("provider_id", "visit_month")
  .agg(count("visit_id").alias("visit_count"))
  .orderBy("provider_id", "visit_month")

// Convert the result to JSON format and save to file
val visitsPerProviderMonthJson = visitsPerProviderMonth.toJSON
visitsPerProviderMonthJson.write.mode("overwrite").json("visits_per_provider_month.json")

spark.stop()
